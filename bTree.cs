﻿using System.Collections;


namespace IEnumerables.BinaryTree;

public class Btree<T> : IEnumerable<T> where T : IComparable<T>
{
    public Btree<T>? Left, Right;

    public T Data;

    public Btree(T value) => Data = value;

    public static Btree<T> Build(Btree<T>? root, T data)
    {
        if (root == null)
            return new Btree<T>(data);

        if (data.CompareTo(root.Data) > 0)
            root.Right = Build(root.Right, data);
        else
            root.Left = Build(root.Left, data);

        return root;
    }

    public static Btree<T> FromRange(Btree<T>? root, IEnumerable<T> data)
    {
        foreach (var value in data)
        {
            if (root == null)
                root = Build(null, value);
            else
                Build(root, value);
        }
        
        return root!;
    }

    public IEnumerator<T> GetEnumerator()
    {
        if (Left != null)
            foreach (var v in Left)
                yield return v;

        yield return Data;

        if (Right != null)
            foreach (var v in Right)
                yield return v;
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
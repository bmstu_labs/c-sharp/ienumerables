﻿using System.Collections;

namespace IEnumerables.UniLinkedList;

public struct UdLinkedList<T>:  IEnumerable<T>
{
    public UdNode<T>? Head { get; set; }
    public UdNode<T>? Last { get; set; }

    public IEnumerable<UdNode<T>> Nodes
    {
        get
        {
            UdNode<T>? cur = Head;

            while (cur != null)
            {
                yield return cur;
                cur = cur.Next;
            }

            yield break;
        }
    }

    public UdNode<T>? Find(T value)
    {
        foreach (var node in Nodes)
        {
            if (node.Value == null)
                continue;
            if (node.Value.Equals(value))
                return node;
        }
        return null;
    }

    public void Add(T value)
    {
        UdNode<T> node = new(value);

        if (Head == null)
            Head = Last = node;
        else
        {
            Last!.Next = node;
            Last = node;
        }
    }

    public void AddRange(IEnumerable<T> items)
    {
        foreach(var item in items)
            Add(item);
    }

    public void Remove(UdNode<T> node)
    {
        UdNode<T>? prev = null;

        foreach (var n in Nodes)
        {
            if (n.Equals(node))
            {
                if (prev != null)
                {
                    prev.Next = n.Next;
                    if (n == Last)
                        Last = prev;
                }
                else
                    Head = n.Next;

                break;
            }
            prev = n;
        }
    }

    public void Remove(T value)
    {
        var find = Find(value);
        if (find != null)
            Remove(find);
    }

    public IEnumerator<T> GetEnumerator()
    {
        return new Enumerator(Head);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    struct Enumerator : IEnumerator<T>
    {
        private UdNode<T>? _head;
        private UdNode<T>? _current;

        public Enumerator(UdNode<T>? head) => _head = head;

        public T Current
        {
            get
            {
                if (_current == null)
                    throw new InvalidOperationException($"Enumeration is not started yet. Call {nameof(MoveNext)}() before accessing {nameof(Current)} property");
                else
                    return _current.Value!;
            }
        }

        object IEnumerator.Current => Current!;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_current == null)
            {
                if (_head == null)
                    return false;

                _current = _head;
                return true;
            }
            else if (_current?.Next == null)
                return false;
            else
            {
                _current = _current.Next;
                return true;
            }
        }

        public void Reset()
        {
            _current = null;
        }
    }
}

public class UdNode<T>
{
    public T? Value { get; set; }

    public UdNode<T>? Next;

    public UdNode(T value) => Value = value;
}

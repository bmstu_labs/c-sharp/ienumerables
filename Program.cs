﻿using IEnumerables.BinaryTree;
using IEnumerables.UniLinkedList;

namespace IEnumerables;

internal class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        Btree<int>? root = Btree<int>.FromRange(null, Enumerable.Range(1, 10));

        var rnd = new Random();
        List<int> a = new();
        
        foreach (var i in Enumerable.Range(1, 20))
            a.Add(rnd.Next(0, 50));

        Btree<int>? root1 = Btree<int>.FromRange(null, a.Distinct());

        Btree<int>? root2 = null;

        foreach (var v in new[] { 8, 3, 10, 1, 6, 14, 4, 7, 13 })
            root2 = Btree<int>.Build(root2, v);

        foreach (var i in root)
            Console.WriteLine(i);

        Console.WriteLine("----------");

        foreach (var i in root1)
            Console.WriteLine(i);

        Console.WriteLine("----------");

        foreach (var i in root2!)
            Console.WriteLine(i);

        UdLinkedList<int?> list = new();
        foreach (int i in Enumerable.Range(1, 10))
            list.Add((i == 5) ? null : i);

        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");
        list.Remove(list.Head!.Next!.Next!);
        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");
        list.Remove(4);
        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");
        list.Remove(list.Head);
        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");
        list.Remove(10);
        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");

        list.AddRange(Enumerable.Range(20, 5).Cast<int?>());
        foreach (int? i in list)
            Console.WriteLine(i);

        Console.WriteLine("----------");
    }
}
